# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

# Configuration functions for CTTag
# Author: Wouter van den Wollenberg (2013-2014)
from BTagging.BTaggingFlags import BTaggingFlags

metaCTTag = { 'IsATagger'          : True,
               'DependsOn'          : ['AtlasExtrapolator',
                                       'BTagTrackToVertexTool',
                               #       'BTagCalibrationBrokerTool',
                                       'IP3DTag',
                                       'JetFitterTagCOMBNN',
                                       'SV1Tag'],
              # 'CalibrationFolders' : ['MV2Tag',],
              # 'PassByPointer'      : {'calibrationTool' : 'BTagCalibrationBrokerTool'},
               'ToolCollection'     : 'MV2Tag' }

def toolCTTag(name, useBTagFlagsDefaults = True, **options):
    """Sets up a CTTag tool and returns it.

    The following options have BTaggingFlags defaults:

    OutputLevel                         default: BTaggingFlags.OutputLevel
    Runmodus                            default: BTaggingFlags.Runmodus
    taggerName                          default: "CTTag"
    taggerNameBase                      default: "CTTag"

    input:             name: The name of the tool (should be unique).
      useBTagFlagsDefaults : Whether to use BTaggingFlags defaults for options that are not specified.
                  **options: Python dictionary with options for the tool.
    output: The actual tool, which can then by added to ToolSvc via ToolSvc += output."""
    if useBTagFlagsDefaults:
        defaults = { 'OutputLevel'                      : BTaggingFlags.OutputLevel,
                     'Runmodus'                         : BTaggingFlags.Runmodus,
                     'taggerName'                       : 'CTTag',
                     'taggerNameBase'                   : 'CTTag' }
        for option in defaults:
            options.setdefault(option, defaults[option])
    options['name'] = name
    from JetTagTools.JetTagToolsConf import Analysis__CTTag
    return Analysis__CTTag(**options)
