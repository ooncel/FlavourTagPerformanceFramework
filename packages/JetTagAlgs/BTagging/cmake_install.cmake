# Install script for directory: /afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/InstallArea/x86_64-slc6-gcc62-opt")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/src/JetTagAlgs/BTagging" TYPE DIRECTORY FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/" USE_SOURCE_PERMISSIONS REGEX "/\\.svn$" EXCLUDE REGEX "/\\.git$" EXCLUDE REGEX "/[^/]*\\~$" EXCLUDE)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process( COMMAND ${CMAKE_COMMAND}
      -E make_directory
      $ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/include )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process( COMMAND ${CMAKE_COMMAND}
         -E create_symlink ../src/JetTagAlgs/BTagging/BTagging
         $ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/include/BTagging )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDebugx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE OPTIONAL FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/lib/libBTaggingLib.so.dbg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY OPTIONAL FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/lib/libBTaggingLib.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libBTaggingLib.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libBTaggingLib.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/atlas.cern.ch/repo/sw/software/21.2/sw/lcg/releases/binutils/2.28/x86_64-slc6/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libBTaggingLib.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDebugx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE OPTIONAL FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/lib/libBTagging.so.dbg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE MODULE OPTIONAL FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/lib/libBTagging.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libBTagging.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libBTagging.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/atlas.cern.ch/repo/sw/software/21.2/sw/lcg/releases/binutils/2.28/x86_64-slc6/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libBTagging.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE OPTIONAL FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConf.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process( COMMAND ${CMAKE_COMMAND} -E touch
      $ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/python/BTagging/__init__.py )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfig.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfig.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_CommonTools.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_CommonTools.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_DL1FlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_DL1FlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_DL1Tag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_DL1Tag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_ExKtbbTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_ExKtbbTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP1DTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP1DTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP2DFlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP2DFlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP2DNegTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP2DNegTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP2DPosTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP2DPosTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP2DSpcFlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP2DSpcFlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP2DSpcNegTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP2DSpcNegTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP2DSpcPosTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP2DSpcPosTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP2DSpcTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP2DSpcTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP2DTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP2DTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP3DFlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP3DFlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP3DNegTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP3DNegTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP3DPosTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP3DPosTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP3DSpcFlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP3DSpcFlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP3DSpcNegTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP3DSpcNegTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP3DSpcPosTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP3DSpcPosTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP3DSpcTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP3DSpcTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_IP3DTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_IP3DTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_InDetVKalVxInJetTool.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_InDetVKalVxInJetTool.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_InDetVKalVxMultiVxInJetTool.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_InDetVKalVxMultiVxInJetTool.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_InDetVKalVxNegativeTagInJetTool.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_InDetVKalVxNegativeTagInJetTool.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_JetProbFlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_JetProbFlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_JetProbTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_JetProbTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_JetVertexCharge.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_JetVertexCharge.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_LoadTools.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_LoadTools.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV1FlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV1FlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV1Tag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV1Tag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV1cFlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV1cFlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV1cTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV1cTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2FlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2FlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2Tag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2Tag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2c00FlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2c00FlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2c00Tag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2c00Tag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2c100FlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2c100FlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2c100Tag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2c100Tag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2c10FlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2c10FlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2c10Tag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2c10Tag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2c10hpFlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2c10hpFlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2c10hpTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2c10hpTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2c10muFlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2c10muFlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2c10muTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2c10muTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2c10rnnFlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2c10rnnFlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2c10rnnTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2c10rnnTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2c20FlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2c20FlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2c20Tag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2c20Tag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2cl100FlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2cl100FlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2cl100Tag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2cl100Tag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2mFlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2mFlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MV2mTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MV2mTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MultiSVTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MultiSVTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MultiSVbb1Tag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MultiSVbb1Tag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MultiSVbb2Tag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MultiSVbb2Tag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MultivariateFlipTagManager.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MultivariateFlipTagManager.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_MultivariateTagManager.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_MultivariateTagManager.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_NewJetFitterCollection.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_NewJetFitterCollection.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_NewJetFitterIP3DNegCollection.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_NewJetFitterIP3DNegCollection.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_RNNIPTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_RNNIPTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_SV0Tag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_SV0Tag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_SV1FlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_SV1FlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_SV1Tag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_SV1Tag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_SV2FlipTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_SV2FlipTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_SV2Tag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_SV2Tag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_SoftElectronTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_SoftElectronTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_SoftMuonTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_SoftMuonTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_SoftMuonTagChi2.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_SoftMuonTagChi2.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_TagNtupleDumper.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_TagNtupleDumper.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_TrackCounting.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_TrackCounting.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_TrackCountingFlip.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_TrackCountingFlip.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingConfiguration_myIPTag.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingConfiguration_myIPTag.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "BTaggingFlags.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/BTaggingFlags.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "IPFordG.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/IPFordG.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "MV2defaultValues.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/MV2defaultValues.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE RENAME "__init__.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/python/__init__.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfig.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_CommonTools.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_DL1FlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_DL1Tag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_ExKtbbTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP1DTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP2DFlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP2DNegTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP2DPosTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP2DSpcFlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP2DSpcNegTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP2DSpcPosTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP2DSpcTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP2DTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP3DFlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP3DNegTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP3DPosTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP3DSpcFlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP3DSpcNegTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP3DSpcPosTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP3DSpcTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_IP3DTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_InDetVKalVxInJetTool.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_InDetVKalVxMultiVxInJetTool.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_InDetVKalVxNegativeTagInJetTool.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_JetProbFlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_JetProbTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_JetVertexCharge.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_LoadTools.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV1FlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV1Tag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV1cFlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV1cTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2FlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2Tag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2c00FlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2c00Tag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2c100FlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2c100Tag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2c10FlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2c10Tag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2c10hpFlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2c10hpTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2c10muFlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2c10muTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2c10rnnFlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2c10rnnTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2c20FlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2c20Tag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2cl100FlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2cl100Tag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2mFlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MV2mTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MultiSVTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MultiSVbb1Tag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MultiSVbb2Tag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MultivariateFlipTagManager.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_MultivariateTagManager.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_NewJetFitterCollection.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_NewJetFitterIP3DNegCollection.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_RNNIPTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_SV0Tag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_SV1FlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_SV1Tag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_SV2FlipTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_SV2Tag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_SoftElectronTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_SoftMuonTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_SoftMuonTagChi2.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_TagNtupleDumper.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_TrackCounting.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_TrackCountingFlip.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingConfiguration_myIPTag.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/BTaggingFlags.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/IPFordG.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/MV2defaultValues.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/BTagging" TYPE FILE FILES "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/FlavourTagPerformanceFramework/x86_64-slc6-gcc62-opt/python/BTagging/__init__.pyc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/jobOptions/BTagging" TYPE FILE RENAME "AtlfReTagging_jobOptions.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/share/AtlfReTagging_jobOptions.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/jobOptions/BTagging" TYPE FILE RENAME "BTagCalibBroker_AODFix_jobOptions.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/share/BTagCalibBroker_AODFix_jobOptions.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/jobOptions/BTagging" TYPE FILE RENAME "BTagCalibBroker_Trig_jobOptions.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/share/BTagCalibBroker_Trig_jobOptions.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/jobOptions/BTagging" TYPE FILE RENAME "BTagCalibBroker_jobOptions.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/share/BTagCalibBroker_jobOptions.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/jobOptions/BTagging" TYPE FILE RENAME "BTagJetFinder_jobOptions.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/share/BTagJetFinder_jobOptions.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/jobOptions/BTagging" TYPE FILE RENAME "BTaggingReconstructionOutputAODList_jobOptions.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/share/BTaggingReconstructionOutputAODList_jobOptions.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/jobOptions/BTagging" TYPE FILE RENAME "BTaggingReconstructionOutputESDList_jobOptions.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/share/BTaggingReconstructionOutputESDList_jobOptions.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/jobOptions/BTagging" TYPE FILE RENAME "BTagging_BaselineTagger.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/share/BTagging_BaselineTagger.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/jobOptions/BTagging" TYPE FILE RENAME "BTagging_BuildAntiKtZ4TrackJets.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/share/BTagging_BuildAntiKtZ4TrackJets.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/jobOptions/BTagging" TYPE FILE RENAME "BTagging_BuildKt6TrackJets.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/share/BTagging_BuildKt6TrackJets.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/jobOptions/BTagging" TYPE FILE RENAME "BTagging_jobOptions.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/share/BTagging_jobOptions.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/jobOptions/BTagging" TYPE FILE RENAME "BTagging_standAlone.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/share/BTagging_standAlone.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/jobOptions/BTagging" TYPE FILE RENAME "CBNT_BTagJetFinder_jobOptions.py" FILES "/afs/cern.ch/work/o/ooncel/ATLAS_Qual/FlavourTagPerformanceFramework/JetTagAlgs/BTagging/share/CBNT_BTagJetFinder_jobOptions.py")
endif()

