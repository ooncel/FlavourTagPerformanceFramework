file(REMOVE_RECURSE
  "../x86_64-slc6-gcc62-opt/include/JetTagTools"
  "../x86_64-slc6-gcc62-opt/python/JetTagTools/__init__.py"
  "../x86_64-slc6-gcc62-opt/share/QGLikelihood_DATA_v0001.root"
  "../x86_64-slc6-gcc62-opt/share/all_Cone4TowerParticleJets_matchQuark_FastVertexFitter_bkg.root"
  "../x86_64-slc6-gcc62-opt/share/all_Cone4TowerParticleJets_matchQuark_FastVertexFitter_sig.root"
  "../x86_64-slc6-gcc62-opt/share/GbbNNweightsFile.txt"
  "CMakeFiles/JetTagToolsComponentsList"
  "../x86_64-slc6-gcc62-opt/lib/libJetTagTools.components"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/JetTagToolsComponentsList.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
