/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#include "JetTagTools/CTTag.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "GeoPrimitives/GeoPrimitives.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/IToolSvc.h"
#include "JetTagInfo/TruthInfo.h"
#include "JetTagTools/HistoHelperRoot.h"
#include "JetTagTools/TrackSelector.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackingPrimitives.h"   // december OGUL
#include "Navigation/NavigationToken.h"
#include "ITrackToVertex/ITrackToVertex.h"
#include "JetTagTools/JetTagUtils.h"
#include "ParticleJetTools/JetFlavourInfo.h"
//#include "InDetVKalVxInJetTool/InDetTrkInJetType.h"  /////////////////////// ADDED OGUL
#include "InDetVKalVxInJetTool/InDetVKalVxInJetTool.h" // OGUL
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"
#include "TrackVertexAssociationTool/ITrackVertexAssociationTool.h"
#include "TH1.h"
#include <cmath>
#include <sstream>
#include <algorithm>
#include <vector>
#include <string>
#include "TLorentzVector.h"

namespace Analysis {

  CTTag::CTTag(const std::string& t, const std::string& n, const IInterface* p)
    : AthAlgTool(t,n,p),
      m_runModus("analysis"),
      m_histoHelper(0),
      m_InDetTrackClassificationTool("InDet::InDetTrkInJetType",this), //ADDED OGUL POST COMPILATION TEST
      m_TightTrackVertexAssociationTool("CP::TightTrackVertexAssociationTool")
  {
    
    declareInterface<ITagTool>(this);
    
    declareProperty("Runmodus"      , m_runModus);
    declareProperty("xAODBaseName"  , m_xAODBaseName);
    declareProperty("defaultvals", m_defaultvals );  
    declareProperty("MVTMvariableNames", m_MVTM_name_tranlations );
    declareProperty("taggerName", m_taggerName = "CTT1");
    declareProperty("taggerNameBase", m_taggerNameBase = "CTT1m10"); 
    declareProperty("RejectBadTracks"     , m_RejectBadTracks     = true);
    declareProperty("checkOverflows"      , m_checkOverflows      = false);
    declareProperty("trackAssociationName"    , m_trackAssociationName = "BTagTrackToJetAssociator");
    declareProperty("originalTPCollectionName", m_originalTPCollectionName = "InDetTrackParticles");
    declareProperty("jetCollectionList"       , m_jetCollectionList);
    declareProperty("referenceType"           , m_referenceType = "ALL"); // B, UDSG, ALL
    declareProperty("truthMatchingName"       , m_truthMatchingName = "TruthInfo");
    declareProperty("purificationDeltaR"      , m_purificationDeltaR = 0.8);
    declareProperty("jetPtMinRef"             , m_jetPtMinRef = 15.*Gaudi::Units::GeV);
    declareProperty("TrackVertexAssociationTool", m_TightTrackVertexAssociationTool); //
  }

  CTTag::~CTTag() {
    delete m_histoHelper;

  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BEGIN:: INITIALIZER
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  StatusCode CTTag::initialize() {

    /** retrieving histoHelper: */
    ITHistSvc* myHistoSvc;
    if( service( "THistSvc", myHistoSvc ).isSuccess() ) {
      ATH_MSG_DEBUG("#BTAG# HistoSvc loaded successfully.");
      m_histoHelper = new HistoHelperRoot(myHistoSvc);
      m_histoHelper->setCheckOverflows(m_checkOverflows);
    } else {
      ATH_MSG_ERROR("#BTAG# HistoSvc could NOT bo loaded.");
    }
    return StatusCode::SUCCESS;
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// END:: INITIALIZER
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  StatusCode CTTag::finalize() {
    return StatusCode::SUCCESS;
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BEGIN:: TRUTH JETS FOR REFERENCE
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  StatusCode CTTag::tagJet(xAOD::Jet& jetToTag, xAOD::BTagging* BTag) {

    ATH_MSG_VERBOSE("###CTTag### tagJet initiated");

    /** author to know which jet algorithm: */
    std::string author = JetTagUtils::getJetAuthor(jetToTag);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BEGIN:: LOOP ON TRACKS AND TRACK SCORE ASSIGNMENT
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    std::vector< ElementLink< xAOD::TrackParticleContainer > > associationLinks = 
    BTag->auxdata<std::vector<ElementLink<xAOD::TrackParticleContainer> > >(m_trackAssociationName);

    if( associationLinks.size() == 0 ) {
        ATH_MSG_DEBUG("###CTT###  Could not find tracks associated with BTagging object as " << m_trackAssociationName);
    } else {
      
      std::vector< ElementLink< xAOD::TrackParticleContainer> >::const_iterator trkIter;

      std::vector<double> myHFTrkScore{0}; // trkTypeWgts[0] score
      std::vector<float> myFGTrkScore{0};  // trkTypeWgts[1] score
      std::vector<float> myGBTrkScore{0};  // trkTypeWgts[2] score

      TLorentzVector myjet; // TLorentz vector for the jet, required as an input for TCT
      myjet.SetPtEtaPhiE(jetToTag.pt(),jetToTag.eta(),jetToTag.phi(),jetToTag.e()); // Assign values to jet

      for( trkIter = associationLinks.begin(); trkIter != associationLinks.end() ; ++trkIter ) { // Start loop over tracks

        const xAOD::TrackParticle* aTemp = **trkIter;

        ATH_MSG_DEBUG("###CTT###  myjet.DeltaR:::" << myjet.DeltaR(aTemp->p4()) );
	
	if(myjet.DeltaR(aTemp->p4()) > 0.4) continue; // cone size

        ATH_MSG_DEBUG("###CTT###  myjet.DeltaR after, in the loop" );

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BEGIN: temporary track quality selection until the TCT update by Vadim
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
	if( aTemp->numberDoF() == 0)    continue; //Protection
	if( aTemp->pt() > myjet.Pt() )     continue; //Protection
	if( aTemp->chiSquared() / aTemp->numberDoF() > 5.)  continue;  //bad track
 
	uint8_t PixelHits,SctHits ;
	if( !aTemp->summaryValue(PixelHits,xAOD::numberOfPixelHits) )   continue;  // no PixelHits info
	if( !aTemp->summaryValue(SctHits,xAOD::numberOfSCTHits)     )   continue;   // no SctHits  info
	if(  SctHits < 5 )        continue;    //bad track
	if( PixelHits <1 )      continue;    //bad track

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// END: temporary track quality selection until the TCT update by Vadim
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	std::vector<float> mytrkRank=m_InDetTrackClassificationTool->trkTypeWgts(aTemp, *m_priVtx, myjet);// track classification tool weight calculator called
        ATH_MSG_DEBUG("####CTT###  myjet.DeltaR in the loop after mytrkrank" );

	myHFTrkScore.push_back((double)mytrkRank[0]); // BTag needs double, casting float -> double for 3 types  
        myFGTrkScore.push_back((double)mytrkRank[1]); 
        myGBTrkScore.push_back((double)mytrkRank[2]); 

        ATH_MSG_DEBUG("###CTT###  myjet.DeltaR in the loop after pushback" );
        
      } // end of for loop on tracks

        ATH_MSG_DEBUG("#############################################CTT###  myjet.DeltaR out of the loop" );
        
	BTag->auxdata<std::vector<ElementLink<xAOD::TrackParticleContainer> > >(m_trackAssociationName);

    ATH_MSG_DEBUG( "[CTTag.cxx] after track assoc loop" );

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// END:: LOOP ON TRACK AND TRACK SCORE ASSIGNMENT
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BEGIN:: FILL INFORMATION TO THE XAOD
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
      ATH_MSG_DEBUG( "#####CTT##### [CTTag.cxx] CTT HF Max_ " << *max_element(std::begin(myHFTrkScore), std::end(myHFTrkScore) ));  
      ATH_MSG_DEBUG( "#####CTT##### [CTTag.cxx] CTT FG Max_ " << *max_element(std::begin(myFGTrkScore), std::end(myFGTrkScore) ));  
      ATH_MSG_DEBUG( "#####CTT##### [CTTag.cxx] CTT GB Max_ " << *max_element(std::begin(myGBTrkScore), std::end(myGBTrkScore) ));  

      BTag->setVariable<double>("CTT1m10", "discriminant", *max_element(std::begin(myHFTrkScore), std::end(myHFTrkScore)));  // Pass the maximum track's score to BTag
      BTag->setVariable<double>("CTT1m20", "discriminant", *max_element(std::begin(myFGTrkScore), std::end(myFGTrkScore)));  // [1] and [2] are not configured properly yet.
      BTag->setVariable<double>("CTT1m30", "discriminant", *max_element(std::begin(myGBTrkScore), std::end(myGBTrkScore)));  // 

} // end of else condition

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// END:: FILL INFORMATION TO THE XAOD
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    return StatusCode::SUCCESS;
  }

}//end namespace

