/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef JETTAGTOOLS_CTTAG_H
#define JETTAGTOOLS_CTTAG_H

/******************************************************
    @class CTTag
    b-jet tagger based on the Track Classification Tool (TCT)
    @author Ogul Oencel -  PI Bonn, Germany
********************************************************/

#include "AthenaBaseComps/AthAlgTool.h"
#include "GaudiKernel/ToolHandle.h"
#include "JetTagTools/ITagTool.h"
#include "xAODTracking/TrackParticleContainerFwd.h"
#include "InDetVKalVxInJetTool/InDetTrkInJetType.h" // ADDED OGUL
#include <vector>

namespace InDet { class IInDetTrkInJetType;}// COMPILED VERSION
namespace CP { class ITrackVertexAssociationTool;}
namespace Analysis { 

  class HistoHelperRoot;
  class CTTag : public AthAlgTool , virtual public ITagTool {
   
  public:
    CTTag(const std::string&,const std::string&,const IInterface*);
      
    virtual ~CTTag();
    StatusCode initialize();
    StatusCode finalize();
      
    /** Set the primary vertex. TODO: This is temporary ! The primary vertex should
	be part of the JetTag IParticle interface implementation. The trouble with 
	ElementLink and persistency has to be solved for that. Revisit ... */
    void setOrigin(const xAOD::Vertex* priVtx);
      
    StatusCode tagJet(xAOD::Jet& jetToTag, xAOD::BTagging * BTag);    
    
    void finalizeHistos() {};
    
  private:      

    /** This switch is needed to indicate what to do. The algorithm can be run to produce
	reference histograms from the given MC files (m_runModus=0) or to work in analysis mode
	(m_runModus=1) where already made reference histograms are read.*/ 
    std::string    m_runModus;          //!< 0=Do not read histos, 1=Read referece histos (analysis mode)

    /** base name string for persistification in xaod */
    std::string m_xAODBaseName;
      
    /** Histogram Helper Class */
    HistoHelperRoot* m_histoHelper;

    std::string m_CTT1mXX;

    bool m_RejectBadTracks;
    bool m_SignWithSvx;
    bool m_checkOverflows;      // if true put the overflows in the first/last bins

    std::string m_taggerName;
    std::string m_taggerNameBase; // unique name for regular and flip versions
    std::string m_treeName;
    std::string m_varStrName;
    std::map<std::string, double > m_defaultvals;
    std::map<std::string, float* > m_local_inputvals;
    std::map<std::string, std::string > m_MVTM_name_tranlations;

    /** Name of the track-to-jet association in the BTagging object */
    std::string m_trackAssociationName;

    /** List of the variables to be used in the likelihood */
    std::vector<std::string> m_useVariables;
     
    /** for reference mode: */
    std::string m_referenceType;     // label to use for reference mode
    std::string m_truthMatchingName; // name of truthMatchingTool instance to get TruthInfo 
    double m_purificationDeltaR;     // skip light jets with heavy flavor in this cone
    double m_jetPtMinRef;            // min cut on jet pT for reference

    /** information to persistify: */
    std::string m_originalTPCollectionName;
    /** Storage for the primary vertex. Can be removed when JetTag provides origin(). */
    // this pointer does not need to be deleted in the destructor (because it points to something in storegate)
    const xAOD::Vertex* m_priVtx = 0;
    std::vector<std::string> m_jetCollectionList, m_jetWithInfoPlus; // 

    ToolHandle< InDet::IInDetTrkInJetType > m_InDetTrackClassificationTool;              /////////////////////// ADDED OGUL

    /** TrackVertex associator (temporary: to be moved to a separate Tool) */
    ToolHandle< CP::ITrackVertexAssociationTool > m_TightTrackVertexAssociationTool;

  }; // End class

  inline void CTTag::setOrigin(const xAOD::Vertex* priVtx) { m_priVtx = priVtx; }

} // End namespace 

#endif
