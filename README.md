Requirements
==================
The following recipe has been tested on LXPLUS. Following these steps will create a b-tagging performance plot for MV2c10, IP3D, SV1 and CTT. 

The samples for running the program needs to be specified in jobOptions file. The detailed information is given in the jobOptions section.

Setup Instructions
==================

```sh
git clone ssh://git@gitlab.cern.ch:7999/ooncel/FlavourTagPerformanceFramework.git
cd FlavourTagPerformanceFramework
git checkout ogul_dev
source scripts/setup.sh
```
jobOptions
==================

Once you've built the framework, go to run/ folder and open jobOptions file,

```sh
cd run
vim jobOptions
```

the samples needs to be specified at the top where FilesInput is defined. In the
following lines two files are shown as an example of how to add a sample,

```sh
jp.AthenaCommonFlags.FilesInput = [
   '/eos/user/j/jlennon/mc16_13TeV.301333.exampleSample1.DAOD.FTAG1.p3543.pool.root.1',
   '/eos/user/j/jlennon/mc16_13TeV.301333.exampleSample2.DAOD.FTAG1.p3543.pool.root.1',
]
```

Retagging framework is sensitive to the derivation tags. Default version
is 21.2.29. It can be changed from the scripts/setup.sh file. The framework
needs to be re-compiled in case of version change. It is not guaranteed that the 
framework will work with a different version.

The tag-version compatibility can be checked at: 
https://twiki.cern.ch/twiki/bin/view/AtlasProtected/DerivationProductionTeam

In 21.2.29, p3543 tag is used.

The log of changes made inbetween different releases can be seen at: 
https://twiki.cern.ch/twiki/bin/view/AtlasComputing/AthDerivationReleasesInfo21_2

The number of events can be set at the line just before the definition of
sample inputs, default value is 1,

```sh
jp.AthenaCommonFlags.EvtMax.set_Value_and_Lock(1)
```



to run:

```sh
cd run
athena jobOptions.py
```

This command will create an output ROOT file, whose name by default is
flav_Akt4EMTo_flat.root

Plotting
==============================

A Plotter can be dowloaded from: /afs/cern.ch/user/o/ooncel/work/public/PLOTTER 

1.1 The Files and Setup The plotter is consistent of following set of files:

    mymap.h This file contains some make pair elements to match a chosen name with a file path.
    mymap.cc: This file relays the map function
    main.cpp: Most of the plotting code is here.
    ATLAS style files: These are called by the program to style the plots.
    bTag_AntiKt4EMTopoJets.C and .h: These are MakeClass files to call branches from the main
    makefile and setup.sh: These for setup and compilation 

1.2 Preparing the Plotter

First you can go to mymap.h and choose some file names of your liking and attach the relevant file paths you want to compare outputs from. You need at least two such map elements, or files.
```
std::make_pair<std::string, std::string>("zprimeflatnov", "/afs/cern.ch/user/o/ooncel/work/ATLAS_Qual/RETAG_MASTER/run/flav_Akt4EMTo_test.root"),
```
Then you can go to main.cpp and update the file name under:
```
vector sample_labels = {"zprimeflatnov", "zprimeflatnov_notct" };
```
and set the number of events you want to run over from; int evtloopmax=4000; If the total number of events are less than specified value, program will run over total number of events.

For plot styling you can change the printouts on the plots from;

        atlasint->AddEntry((TObject*)0, "#it{#scale[1.2]{ATLAS}} #bf{Internal}", "");
        selection->AddEntry((TObject*)0,"#bf{Z' Flat (427080)}", "");
        selection2->AddEntry((TObject*)0,"#bf{p^{jet}_{T}>1000 GeV, |#eta^{jet}|<2.5, N=20K}", "");
        selection3->AddEntry((TObject*)0,"#sqrt{s}=13 TeV", "");

You can edit the range of ratio plot at the bottom from;
```
ratC->SetMaximum(1.7);
ratC->SetMinimum(0.5);
```
1.3 Running the Plotter

To run the plotter do the following
```
source setup.sh
make clean
make
./main
```


To submit samples on the grid:
==============================

To make GRID submission, you need to first setup Panda and Rucio

```
setupATLAS
lsetup panda rucio
say "y" and press enter to the RUCIO_ACCOUNT question
Type "voms-proxy-init -voms atlas" and press enter
Enter your password and press enter
```

The submission to GRID is then straightforward, again from the /run folder one can simply use the following command:
```
pathena btagAnalysis/myrun.py --inDS mc16_13TeV.301333.Pythia8EvtGen_A14NNPDF23LO_zprime3000_tt.deriv.DAOD_FTAG1.e3723_s3126_r10201_p3543 --outDS user.ooncel.mc16_13TeV.301333.Py8EvtGen_A14NNPDF23LO_zprime3000_tt.FTAG1.ogulnew_Akt4EMTo
```
Here the long file names coming after --inDS and --outDS are given as examples for zprime3000_tt sample and needs to be
changed according to your needs. Some documentation regarding Pathena usage can be found in Pathena TWiki page: PandaAthena. 
After the submission you can follow your file from https://bigpanda.cern.ch 


