##########################################################################################################################################################
#OPTIONS AND MV2DEFAULT VALUES
##########################################################################################################################################################

doRetag = True
default_values = {
"ip2_pu"                      : -1,
"ip2_pb"                      : -1,
"ip2_pc"                      : -1,
"ip3_pu"                      : -1,
"ip3_pb"                      : -1,
"ip3_pc"                      : -1,
"ip2"                         : -30,
"ip2_c"                       : -30,
"ip2_cu"                      : -30,
"ip3"                         : -30,
"ip3_c"                       : -30,
"ip3_cu"                      : -30,
"sv1"                         : -10,
"sv1_c"                       : -10,
"sv1_cu"                      : -10,
"sv1_ntkv"                    : -1,
"sv1_mass"                    :  -1000,
"sv1_efrc"                    :  -1,
"sv1_n2t"                     :  -1,
"sv1_Lxy"                     :  -100,
"sv1_L3d"                     :  -100,
"sv1_sig3"                    :  -100,
"sv1_dR"                      :   -1,
"sv1_distmatlay"              :  -1,
"jf_mass"                     :  -1000,
"jf_efrc"                     : -1,
"jf_n2tv"                     : -1,
"jf_ntrkv"                    :  -1,
"jf_nvtx"                     : -1,
"jf_nvtx1t"                   :  -1,
"jf_dphi"                     :  -11,
"jf_deta"                     :  -11,
"jf_dR"                       : -1,
"jf_sig3"                     : -100,
"jf_dR_flight"                : -1,
"jf_mass_unco"                : -1000,
"width"                       : 0.0,
"n_trk_sigd0cut"              : 0.0,
"trk3_d0sig"                  :  -100,
"trk3_z0sig"                  :  -100,
"sv_scaled_efc"               : -1,
"jf_scaled_efc"               : -1,
"nTrk_vtx1"                   : 0.0,
"mass_first_vtx"              : -100,
"e_first_vtx"                 : -100,
"e_frac_vtx1"                 : 0,
"closestVtx_L3D"              : -10,
"JF_Lxy1"                     : -5,
"vtx1_MaxTrkRapidity" 		  : 0.0,
"vtx1_AvgTrkRapidity" 		  : 0.0,
"vtx1_MinTrkRapidity" 		  : 0.0,
"MaxTrkRapidity" 		      : 0.0,
"MinTrkRapidity" 		      : 0.0,
"AvgTrkRapidity" 		      : 0.0,
"my_smt"                      : -1,
"sm_mu_pt"                    :  -1,
"sm_dR"                       :  -1,
"sm_qOverPratio"              :  -99,
"sm_mombalsignif"             :  -99,
"sm_scatneighsignif"          :  -99,
"sm_pTrel"                    :  -99,
"sm_mu_d0"                    :  -99,
"sm_mu_z0"                    :  -99,
"sm_ID_qOverP"                :  -99,
"rnnip_pb"                   : -20,
"rnnip_pc"                   : -20,
"rnnip_pu"                   : -20,
"rnnip_ptau"                   : -20,
}

JetCollections = [
  ##"AntiKt10LCTopoJets"
  # 'AntiKtVR30Rmax4Rmin02TrackJets',
  #'AntiKt2PV0TrackJets',
  #'AntiKtVR30Rmax4Rmin02TrackJets',
  #'AntiKt2PV0TrackJets',
  'AntiKt4EMTopoJets',
  #'AntiKt4EMPFlowJets',
  #'AntiKt4PV0TrackJets',
  #'AntiKt3PV0TrackJets',
  #'AntiKt4LCTopoJets',
  ]

#########################################################################################################################################################
#########################################################################################################################################################
### Define input xAOD and output ntuple file name
import glob
from AthenaCommon.AthenaCommonFlags import jobproperties as jp
#jp.AthenaCommonFlags.EvtMax.set_Value_and_Lock( vars().get('EVTMAX', -1) )
#jp.AthenaCommonFlags.SkipEvents.set_Value_and_Lock(16400)
jp.AthenaCommonFlags.EvtMax.set_Value_and_Lock(10)

jp.AthenaCommonFlags.FilesInput = ['/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000001.pool.root.1','/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000002.pool.root.1','/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000003.pool.root.1','/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000004.pool.root.1'
,'/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000005.pool.root.1','/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000006.pool.root.1','/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000007.pool.root.1','/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000008.pool.root.1','/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000009.pool.root.1','/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000010.pool.root.1','/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000011.pool.root.1','/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000012.pool.root.1','/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000013.pool.root.1','/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000014.pool.root.1','/eos/user/o/ooncel/CTT/zpflat/DAOD_FTAG1.17009230._000015.pool.root.1']


#'/eos/user/o/ooncel/mc16_13TeV.301331.Pythia8EvtGen_A14NNPDF23LO_zprime2500_tt.deriv.DAOD_FTAG1.e4061_s3126_r10201_p3543/DAOD_FTAG1.14051376._000010.pool.root.1',
# from PyUtils import AthFile
# af = AthFile.fopen( jp.AthenaCommonFlags.FilesInput()[0] )

evtPrintoutInterval = vars().get('EVTPRINT', 1)
svcMgr += CfgMgr.AthenaEventLoopMgr( EventPrintoutInterval=evtPrintoutInterval )

svcMgr += CfgMgr.THistSvc()

for jet in JetCollections:

  shortJetName=jet.replace("AntiKt","Akt").replace("TopoJets","To").replace("TrackJets","Tr").replace("PFlowJets","Pf")
  svcMgr.THistSvc.Output += [ shortJetName+" DATAFILE='flav_"+shortJetName+"_test.root' OPT='RECREATE'"]

##########################################################################################################################################################
##########################################################################################################################################################

from RecExConfig.RecFlags import rec
rec.doESD.set_Value_and_Lock        (False)
rec.doWriteESD.set_Value_and_Lock   (False)
rec.doAOD.set_Value_and_Lock        (False)
rec.doWriteAOD.set_Value_and_Lock   (False)
rec.doWriteTAG.set_Value_and_Lock   (False)
rec.doDPD.set_Value_and_Lock        (False)
rec.doTruth.set_Value_and_Lock      (False)


rec.doApplyAODFix.set_Value_and_Lock(False)
include ("RecExCommon/RecExCommon_topOptions.py")

from AthenaCommon.AlgSequence import AlgSequence
algSeq = AlgSequence()

##########################################################################################################################################################
##########################################################################################################################################################
### GEO Business
from AthenaCommon.GlobalFlags import globalflags
print "detDescr from global flags= "+str(globalflags.DetDescrVersion)
from AtlasGeoModel.InDetGMJobProperties import GeometryFlags as geoFlags
print "geoFlags.Run()   = "+geoFlags.Run()
print "geoFlags.isIBL() = "+str(  geoFlags.isIBL() )


##########################################################################################################################################################
##########################################################################################################################################################
### this is if you want to re-tag with another calibration file
from BTagging.BTaggingFlags import BTaggingFlags

#BTaggingFlags.ForceMV2CalibrationAlias = False
#BTaggingFlags.CalibrationChannelAliases += ["AntiKt2PV0Track->AntiKt2Track"]


BTaggingFlags.OutputLevel = INFO

#### if the new file is already in the datatbase: simple edit the name
#BTaggingFlags.CalibrationTag = 'BTagCalibRUN12-08-42'

#### if you want to use your own calibration file use this part below
######BTaggingFlags.CalibrationFromLocalReplica = True
######BTaggingFlags.CalibrationFolderRoot = '/GLOBAL/BTagCalib/'
######BTaggingFlags.CalibrationTag = "antonello"
######BTaggingFlags.JetVertexCharge=False

include("btagAnalysis/RetagFragment.py")

##########################################################################################################################################################
### Tools

jvt = CfgMgr.JetVertexTaggerTool('JVT')
ToolSvc += jvt

ToolSvc += CfgMgr.CP__PileupReweightingTool("prw",
                                            OutputLevel = INFO,
                                            UsePeriodConfig= "MC16"
                                            )
#indetvk = CfgMgr.InDetVKalVxInJetTool('InDetVk')
#ToolSvc += indetvk

#from btagAnalysis.btagAnalysisConf import *

from TrkVertexFitterUtils.TrkVertexFitterUtilsConf import Trk__TrackToVertexIPEstimator
ToolSvc+=Trk__TrackToVertexIPEstimator("trkIPEstimator")

#from InDetVKalVxInJetTool.InDetVKalVxInJetToolConf import InDet__InDetTrkInJetType # ADDED OGUL
#ToolSvc+=InDet__InDetTrkInJetType("trkClassificator") # ADDED OGUL

# For running on xAODs without truth jets, un-comment these 2 lines:
#from DerivationFrameworkMCTruth.MCTruthCommon import addStandardTruthContents
#addStandardTruthContents()

##########################################################################################################################################################

algSeq += CfgMgr.BTagVertexAugmenter()

### Main Ntuple Dumper Algorithm
for JetCollection in JetCollections:


  shortJetName=JetCollection.replace("AntiKt","Akt").replace("TopoJets","To").replace("TrackJets","Tr").replace("PFlowJets","Pf")
  alg = CfgMgr.btagAnalysisAlg("BTagDumpAlg_"+JetCollection,
                                  OutputLevel=INFO, #DEBUG
                                  Stream=shortJetName,
                                  JVTtool=ToolSvc.JVT,
                                  )


  alg.JetCollectionName = JetCollection
  alg.doJVT = True #if this is false JVT is NAN, if true an attempt is made to update JVT after calibration


  alg.DefaultValDictionary = default_values
  alg.ReplaceNanDefaults = True

  if "TrackJets" in JetCollection or "Truth" in JetCollection:

    alg.CleanJets     = True
    alg.CalibrateJets = True
    alg.doJVT = True

  alg.JetCleaningTool.CutLevel= "LooseBad"
  alg.JetCleaningTool.DoUgly  = True



## what to include in ntuple ####
  #example
  #alg.exampleBranchInfo = False

  alg.EventInfo = True
  alg.retriveTruthJets = True
  # Flag for truth jet collection to save
  alg.TruthJetCollection = "AntiKt4TruthJets" #(default)
  #alg.TruthJetCollection = "AntiKt4TruthWZJets" # only works on AOD, for use with b-jet energy regression
  alg.JetProperties = True
  #taggers (MV2, DL1)
  alg.TaggerScores = True
  ##IPxD+RNNIP
  alg.ImpactParameterInfo = True
  ##SV1
  alg.SVInfo = True
  alg.svxCollections = {'sv1_': 'SV1'}
  #alg.svxCollections = {'jet_sv1_': 'SV1'}
  
  ##JetFitter
  alg.JetFitterInfo = True
  ###SoftMuonTagger
  alg.SoftMuoninfo = False
  ## b and c hadron truth info
  alg.bHadInfo = True
  alg.bHadExtraInfo = True #include all b and c decay products, and trk_origin
  ## kshort
  alg.kshortInfo = True

  #show debug info for branches
  alg.branchDebug = False

  #track information
  alg.TrackInfo = True
  alg.nRequiredSiHits = 2 #number of hits required to save a track
  alg.TrackCovariance = True

  #you can disable the track augmenter if youre not filling the track branches
  algSeq += CfgMgr.BTagTrackAugmenter(
   "BTagTrackAugmenter_" + JetCollection,
   OutputLevel=INFO,
   JetCollectionName = JetCollection,
   TrackToVertexIPEstimator = ToolSvc.trkIPEstimator,
   #InDetTrkInJetType = ToolSvc.trkClassificator,
   SaveTrackVectors = True,
  )

  alg.AccessBtagObject = True # for fatjets, turn this to False

  algSeq += alg

  #from btagAnalysis.configHelpers import get_calibration_tool
  #ToolSvc += get_calibration_tool(CfgMgr, JetCollection, False)

  from btagAnalysis.configHelpers import get_calibration_tool_2016_calib
  ToolSvc += get_calibration_tool_2016_calib(CfgMgr, JetCollection, False)


from PerfMonComps.PerfMonFlags import jobproperties as PerfMon_jp
PerfMon_jp.PerfMonFlags.doMonitoring = True
PerfMon_jp.PerfMonFlags.doFastMon = True

###########################################################################################################################################################################
