#ifndef TAGGERSCORE_BRANCH_BUFFER_HH
#define TAGGERSCORE_BRANCH_BUFFER_HH

#include <vector>

struct TaggerScoreBranchBuffer {

	std::vector<double> *v_jet_dl1_pb;
	std::vector<double> *v_jet_dl1_pc;
	std::vector<double> *v_jet_dl1_pu;
	std::vector<double> *v_jet_dl1mu_pb;
	std::vector<double> *v_jet_dl1mu_pc;
	std::vector<double> *v_jet_dl1mu_pu;
	std::vector<double> *v_jet_dl1rnn_pb;
	std::vector<double> *v_jet_dl1rnn_pc;
	std::vector<double> *v_jet_dl1rnn_pu;
	std::vector<double> *v_jet_mv2c10;
	std::vector<double> *v_jet_ctt1m10; // ADDED OGUL
	std::vector<double> *v_jet_ctt1m20; // ADDED OGUL
	std::vector<double> *v_jet_ctt1m30; // ADDED OGUL
	std::vector<double> *v_jet_mv2c10mu;
	std::vector<double> *v_jet_mv2c10rnn;
	std::vector<double> *v_jet_mv2c100;
	std::vector<double> *v_jet_mv2cl100;

};

#endif // TAGGERSCORE_BRANCH_BUFFER_HH
