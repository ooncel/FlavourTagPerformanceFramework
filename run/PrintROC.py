################################################################################################################################################
#
# B-Tagging Efficiency Plotter
#
################################################################################################################################################
# This code is originally developed by vdao and available at the address
# https://svnweb.cern.ch/trac/atlasperf/browser/CombPerf/FlavorTag/FlavourTagPerformanceFramework/trunk/xAODAthena/btagIBLAnalysis/macros/PrintROC.py
#
# Changes to original code:
# *AtlasLabels implemented (Oct. 2018)
# *Detailed commentary added (Oct. 2018) 
# *Error messages improved (Oct. 2018) 
# *Parts related to 8TeV removed and simplified (Oct. 2018) 
#
# Contact: Ogul Oncel(University of Bonn) - ogul.oncel@cern.ch
################################################################################################################################################

#import argparse
from glob import glob
import os, sys
from array import *
from math import sqrt # Import square root function
import math
import time
#Add AtlasStyle
#For downloading and more information on AtlasStyle: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PubComPlotStyle
from ROOT import *
#gROOT.SetStyle('ATLAS')
gROOT.LoadMacro("AtlasStyle.C") #AtlasStyle.C and AtlasStyle.h has to be in the main folder. If you are having trouble with loading "gROOT.LoadMac.." try "ROOT.gROOT.LoadMac.." 
gROOT.LoadMacro("AtlasUtils.C") #AtlasUtils.C and AtlasUtils.h has to be in the main folder. 
gROOT.LoadMacro("AtlasLabels.C") #AtlasLabels.C and AtlasLabels.h has to be in the main folder. 

CRED = '\033[91m'
CEND = '\033[0m'
CITALIC   = '\33[3m'
CURL      = '\33[4m'
CBLINK    = '\33[5m'
CBLINK2   = '\33[6m'
CSELECTED = '\33[7m'

CBLACK  = '\33[30m'
CRED    = '\33[31m'
CGREEN  = '\33[32m'
CYELLOW = '\33[33m'
CBLUE   = '\33[34m'
CVIOLET = '\33[35m'
CBEIGE  = '\33[36m'
CWHITE  = '\33[37m'

CBLACKBG  = '\33[40m'
CREDBG    = '\33[41m'
CGREENBG  = '\33[42m'
CYELLOWBG = '\33[43m'
CBLUEBG   = '\33[44m'
CVIOLETBG = '\33[45m'
CBEIGEBG  = '\33[46m'
CWHITEBG  = '\33[47m'

CGREY    = '\33[90m'
CRED2    = '\33[91m'
CGREEN2  = '\33[92m'
CYELLOW2 = '\33[93m'
CBLUE2   = '\33[94m'
CVIOLET2 = '\33[95m'
CBEIGE2  = '\33[96m'
CWHITE2  = '\33[97m'

CGREYBG    = '\33[100m'
CREDBG2    = '\33[101m'
CGREENBG2  = '\33[102m'
CYELLOWBG2 = '\33[103m'
CBLUEBG2   = '\33[104m'
CVIOLETBG2 = '\33[105m'
CBEIGEBG2  = '\33[106m'
CWHITEBG2  = '\33[107m'


tmpVale=sys.argv #sys.argv is a list of command line arguments passed to the script.

# The 'len' command counts these arguments. In this script there are three command-line arguments needed: 
# (1) PrintROC.py
# (2) Outputfolder name
# (3) Input root file name. This file should contain proper variables for b-tagging plotting.
#
# Example of successful command-line input: python PrintROC.py myoutputfolder myntuple.root
#
# For example if you do not define the output folder name in the execute command you will have len(sys.argv)=2 instead of 3. In the
# following 'def Helper()' function, a printout is defined, which is to be displayed in case the 'if len' condition, coming after the
# function definition, is not equal to 3; signaling a wrong execute command input.  

#Define a helper function
def Helper():
    print " "
    print (CRED+"***ERROR: We require exactly three command-line input arguments."+CEND)
    print (CGREEN+"***HELP: Example command: python PrintROC.py <outputFolder> <thebeatles.root> "+CEND)
    print " "
    sys.exit(1)

if len(sys.argv) !=3: Helper() #If number of command-line input arguments are different than 3, call the Helper() function.

#The following conditions define the type of output to be displayed depending on the position and content of the individual command-line arguments

odir = sys.argv[1] #Take the first command-line argument

if ".root" in odir: #If this argument contains ".root" in it
    print " "
    print(CRED+ "***ERROR: Output folder "+CWHITE+odir+CRED+" is a ROOT file. I need a normal folder to put my files into. For example you can create a folder by typing: "+CWHITE+"mkdir michelangelo"+CEND)
    print " "
    sys.exit(1) #Exit the program

second=sys.argv[2] #Take the second command-line argument
fileList=[] #Initiate a file list

if ".root" in second: #If this argument contains ".root" in it
    test=TFile(second) #This is a root file and assign it to a variable called "test"
    if test==None: #If the file is not present
        print " ..... input file: "+test+" ... NOT FOUND" #Print this error message
        sys.exit(1) #Exit the program
    fileList.append(second) #Add the file into the previously initialized file list. 
else: #If there is no ".root" in the second command-line argument

    print " "
    print(CRED+ "***ERROR: No ROOT file selected. "+CWHITE+second+CRED+" is not a ROOT file. Please check."+CEND)
    print " "
    print(CGREEN+ "***REMINDER: You have the following ROOT files in the current directory:"+CEND)
    
    currentDir = cwd = os.getcwd() #Get the current directory address and pass it to the currentDir
    dirs = os.listdir( currentDir ) #List the files at the current directory

    #Print all the files and directories with a .root file extension
    for file in dirs:
	if second+".root" in file:
	   
           print(CGREEN+"***REMINDER: Hmm.. I found a Root file with the same name: "+file+" Maybe you forgot to add the .root file extension? Other files available are listed below: "+CEND)
        elif ".root" in file:

           print(CGREEN+"***REMINDER: "+file+CEND)
    print(" ") 
    sys.exit(1) #Exit the program
    
print " "
print(CGREEN + "***INFO: Command-line inputs have been successfuly entered. Executing the program =)" + CEND)
print " "

time.sleep(2) #Wait for 2 second
SetAtlasStyle() #This command activates the AtlasStyle settings and will produce a print out.

# Run ROOT in the Batch mode. Why? Otherwise ROOT will produce pop-up plots on the go. Sometimes it
# might not able to access to graphic display and program might fail.  
gROOT.SetBatch(True)   

taggers=[] #Initialize list of b-taggers

# List of b-taggers and their parameters to be used. Currently unused ones are commented.
# The brackets stand for ["given name for plot legend" , "correct branch name of the variable" , "xmin" , "xmax"  , "number of bins" , "line color"]
taggers=[ ["MV2c10"   , "mv2c10"  ,  -10.0   ,  10.0 ,  2000, 4 ],
           ["IP3D"    , "ip3d"    , -110.   ,   110,  2000, 8 ],
           ["SV1"     , "sv1"     ,  -100.   ,   110,  2000, 6 ],
           ["CTT1m10" , "ctt1m10"     ,   -3.0  , 3.0  , 2000 , 1 ],
#          ["MV1c"    , "mv1c"    ,   0  ,  1  ,  2000, 3 ],
#          ["MV2c00"  , "mv2c00"  ,  -0.5,  0.5,  2000, 2 ],
#          ["MV2c10"  , "mv2c10"  ,  -0.5,  0.5,  2000, 4 ],
#          ["MV2c20"  , "mv2c20"  ,  -0.5,  0.5,  2000, 7 ],
#          ["MVb"   , "mvb"       ,  -1.05 ,  0.8,  2000, 920 ],
          ]

effThreshold=0.70 #Define efficiency threshold.

jetptcut=">1000" 



#Define Histogram for mv2c10 BDT plot
def GetHisto(tag, intree, val):
    tmpH=TH1F(tag[1]+str(val),tag[1]+str(tag[5]),tag[4],tag[2],tag[3]) #Structure of TH1F object (name, title, no.ofbins, xmin, xmax)
    tmpH.Sumw2()
    if "mv2c10" in tag[1]:
      var="jet_"+tag[1]+">>"+tmpH.GetName() #jet_mv2c10>>MV2c10 This is method used in ROOT Draw function i.e. Draw(Variable>>Histogram,cut)
    elif "ctt1m10" in tag[1]:
      var="jet_"+tag[1]+">>"+tmpH.GetName() #jet_mv2c10>>MV2c10 This is method used in ROOT Draw function i.e. Draw(Variable>>Histogram,cut)
    elif "ip3d" in tag[1]:
      var="jet_"+tag[1]+"_llr"+">>"+tmpH.GetName() #jet_ip3d>>MV2c10 This is method used in ROOT Draw function i.e. Draw(Variable>>Histogram,cut)
    elif "sv1" in tag[1]:
      #var="jet_"+tag[1]+"_llr"+">>"+tmpH.GetName() #sv1_llr>> This is method used in ROOT Draw function i.e. Draw(Variable>>Histogram,cut)
      var="sv1_"+tag[1]+"_llr"+">>"+tmpH.GetName() #sv1_llr>> This is method used in ROOT Draw function i.e. Draw(Variable>>Histogram,cut)
    


    print var
    cut=""

    cut=" jet_LabDr_HadF=="+str(val)+" && abs(jet_eta)<2.5 && jet_pt"+jetptcut+"e3 && jet_aliveAfterOR==1 "

    intree.Draw( var, cut,"goof") ##,1000000)
    #intree.Draw( var, cut) ##,1000000)
    tmpH.SetBinContent(1,tmpH.GetBinContent(1)+tmpH.GetBinContent(0))
    tmpH.SetBinError(1,sqrt(pow(tmpH.GetBinError(1),2)+pow(tmpH.GetBinError(0),2)))
    tmpH.SetBinContent(0,0.0)
    tmpH.SetBinError(0,0.0)
    tmpH.SetBinContent(tmpH.GetNbinsX(),tmpH.GetBinContent(tmpH.GetNbinsX())+tmpH.GetBinContent(tmpH.GetNbinsX()+1))
    tmpH.SetBinError(tmpH.GetNbinsX(),sqrt(pow(tmpH.GetBinError(tmpH.GetNbinsX()),2)+pow(tmpH.GetBinError(tmpH.GetNbinsX()+1),2)))
    tmpH.SetBinContent(tmpH.GetNbinsX()+1,0.0)
    tmpH.SetBinError(tmpH.GetNbinsX()+1,0.0)
    if val!=4:
        c = TCanvas("_Discr","_Discr",800,600)    
        tmpH.GetXaxis().SetTitle(tag[0])
        tmpH.Draw("")
        c.Update()
        c.Print(odir+"/"+tag[1]+"_test.eps")
        time.sleep(0.5)
    tmpInt=tmpH.Integral(-10,tmpH.GetNbinsX()+10)
    if tmpH.Integral():
        tmpH.Scale(1./tmpH.Integral(-10,tmpH.GetNbinsX()+10))
    else: print tmpH.Integral(-10,tmpH.GetNbinsX()+10)
    return tmpH,tmpInt

#Define ROC plot. This uses the GetHisto() function defined above to produce a ROC plot.
def GetROC(tag,intree, bVSlight):
    intSig=-1
    intBkgd=-1
    hsig,intSig=GetHisto(tag, intree,5)
    hbkgd=None
    if not bVSlight: hbkgd,intBkgd=GetHisto(tag, intree,4)
    else           : hbkgd,intBkgd=GetHisto(tag, intree,0)

    if bVSlight:
        found=False
        for bin in xrange(1,hsig.GetNbinsX()+2):
            partInt=hsig.Integral(bin,hsig.GetNbinsX()+10)
            
            mybkgdEff=hbkgd.Integral(bin,hsig.GetNbinsX()+10)
            ##print str(bin)+"    "+str(partInt)
            if partInt<effThreshold and not found:
                print " "
                print(CGREEN+ "***INFO: Your selected efficiency threshold is "+CWHITE+str(effThreshold)+CGREEN+" and the cut at "+CWHITE+str(hsig.GetBinCenter(bin))+CGREEN+" has an efficiency of "+CWHITE+str(partInt)+CGREEN+"__bkgeff:__"+CWHITE+str(mybkgdEff)+CEND)
                print(CGREEN+ "***INFO::___1/bkgeff___"+CWHITE+str(1/mybkgdEff)+CEND)
                print " "
                found=True
                
    myROC=TGraphErrors();##hsig.GetNbinsX()-2 );
    maxRej=1
    count=-1
    for bin in xrange(2,hsig.GetNbinsX()):
    #for bin in xrange(1,hsig.GetNbinsX()+2):
        sigEff =hsig.Integral(bin,hsig.GetNbinsX()+10)
        bkgdEff=hbkgd.Integral(bin,hsig.GetNbinsX()+10)
        ##if bVSlight: print str(bkgdEff)+"   "+str(sigEff)+" CUT: "+str(hsig.GetBinCenter(bin))
        if bkgdEff!=0 and sigEff!=0 and sigEff<0.99:
            ex=0
            #ex=sqrt( sigEff*(1-sigEff)/intSig )
            ey=0
            #ey=sqrt( bkgdEff*(1-bkgdEff)/intBkgd )
            ##if bVSlight: print str(bkgdEff)+"   "+str(sigEff)
            count+=1
            myROC.SetPoint(count,sigEff,1/bkgdEff);
            myROC.SetPointError(count, ex, ey/(bkgdEff*bkgdEff) )
            if 1/bkgdEff>maxRej: maxRej=1/bkgdEff
	    if sigEff>0.69 and sigEff<0.71: print("sigeff:"+str(sigEff)+"__rejection__"+str(1/bkgdEff))
    myROC.SetLineWidth(3)
    return myROC,maxRej
  
#########################################################################################
gSystem.Exec("mkdir -p "+odir)

intree=None
intree=TChain("bTag_AntiKt4EMTopoJets")
##if "EM" in odir: intree=TChain("bTag_AntiKt4EMTopoJets")
##else           : intree=TChain("bTag_AntiKt4LCTopoJets")
for file in fileList:
    intree.Add(file)

#Print total number of entries.
print "    "
print(CGREEN+ "***INFO: Total Number of Entries: "+CWHITE+str(intree.GetEntries())+CEND)
print "    "

countT=-1
light=TH1F("b VS light","b VS light",200,0,1);
light.SetTitle(";b-jet efficiency;light-jet rejection;")
lightCurve=[]

cj=TH1F("b VS c","b VS c",200,0,1);
cj.SetTitle(";b-jet efficiency;c-jet rejection;")
cCurve=[]

#Generate the plot for each tagger specified in taggers file.
for tag in taggers:
    countT+=1
    print " "
    print(CGREEN+ "***INFO: Generating plots for "+CWHITE+tag[0]+CGREEN+" tagger.."+CEND)
    print " "
    curve,Rej=GetROC(tag,intree,True)
    curve.SetLineColor(tag[5])
    lightCurve.append(curve)
    if Rej*2>light.GetMaximum(): light.SetMaximum(Rej*2)

    curve2,Rej=GetROC(tag,intree,False)
    curve2.SetLineColor(tag[5])
    cCurve.append(curve2)
    if Rej*2>cj.GetMaximum(): cj.SetMaximum(Rej*2)

ofile=TFile(odir+"/output.root","RECREATE")        

myC=TCanvas( "bVSl", "bVSl",900,900);
myC.SetLogy()
myC.SetGridy()
myC.SetGridx()
light.SetMinimum(1)
light.SetMaximum(1e5)
light.Draw()

# This line requires AtlasLabels.C. Its structure is (x-coord,y-coord,"mytext").
#  It creates properly styled "ATLAS mytext" for the plots at the specified coordinates.
ATLASLabel(0.2,0.85,"Internal");


#altgr+3 for # symbol in turkish keyboard.


legend4=TLegend(0.70,0.70,0.92,0.92)
legend4.SetTextFont(42)
legend4.SetTextSize(0.04)
legend4.SetFillColor(0)
legend4.SetLineColor(0)
legend4.SetFillStyle(0)
legend4.SetBorderSize(0)

legend5=TLegend(0.2,0.77,0.2,0.85)
legend5.SetTextFont(62)
legend5.SetTextColor(1)
legend5.SetTextSize(0.03)
legend5.SetFillColor(0)
legend5.SetLineColor(1)
legend5.SetBorderSize(0)


legend6=TLegend(0.2,0.73,0.2,0.77)
legend6.SetTextFont(62)
legend6.SetTextColor(1)
legend6.SetTextSize(0.03)
legend6.SetFillColor(0)
legend6.SetLineColor(1)
legend6.SetBorderSize(0)



count=-1
for curve in lightCurve: #***HELP: If you are having error here, close the terminal, open another terminal and re-try.
    curve.Draw("C")
    count+=1
    legend4.AddEntry(curve,taggers[count][0],"L")
    #ofile.WriteObject(curve,taggers[count][0].replace("+","_")+"---bl")
    curve.SetName(taggers[count][0].replace("+","_")+"---bl")
    curve.Write()

legend4.Draw()
#splitline{The Data }{slope something }
#legend5.AddEntry("","Jet pT"+jetptcut+"GeV, OLD InDetVKalVxInJetTool","")
legend5.AddEntry("","Jet pT"+jetptcut+" GeV","")
legend6.AddEntry("","NEW InDetVKalVxInJetTool","")
legend5.Draw()
legend6.Draw()

myC.Update()
myC.Print(odir+"/bVSlight.eps")
myC.Print(odir+"/bVSlight.png")
#myC.Print(odir+"/bVSlight.C")

myC2=TCanvas( "cVSl", "cVSl",900,900);
myC2.SetLogy()
myC2.SetGridy()
myC2.SetGridx()
cj.SetMinimum(1)
cj.SetMaximum(1e3)
cj.Draw()
count=-1
for curve in cCurve:
    curve.Draw("C")
    count+=1
    ofile.WriteObject(curve,taggers[count][0].replace("+","_")+"---bc")
legend4.Draw()
myC2.Update()
myC2.Print(odir+"/cVSlight.eps")
myC2.Print(odir+"/cVSlight.png")
#myC2.Print(odir+"/cVSlight.C")

ofile.Close()
print " "
print(CGREEN+"***INFO: Program has been successfully executed. Your output is saved into the "+CWHITE+odir+CGREEN+" folder. You can view your .eps plots from terminal, for example by typing "+CWHITE+"evince "+odir+"/bVSlight.eps"+CGREEN+". I hope you got nice plots! =)"+CEND)
print(CGREEN+"***INFO: If you are working on remote and want to download the plots to your local machine please learn about "+CWHITE+"scp"+CGREEN+" command."+CEND)
print " "

################################################################################################################################################
#
# End of the program
#
################################################################################################################################################
