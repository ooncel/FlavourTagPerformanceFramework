#import argparse
from glob import glob
import os, sys
from array import *
from ROOT import *#TCanvas,TLegend,gROOT
import math
import time

gROOT.LoadMacro("AtlasStyle.C") #AtlasStyle.C and AtlasStyle.h has to be in the main folder. If you are having trouble with loading "gROOT.LoadMac.." try "ROOT.gROOT.LoadMac.." 
gROOT.LoadMacro("AtlasUtils.C") #AtlasUtils.C and AtlasUtils.h has to be in the main folder. 
gROOT.LoadMacro("AtlasLabels.C") #AtlasLabels.C and AtlasLabels.h has to be in the main folder. 

#ROOT.gROOT.LoadMacro("AtlasStyle.C")
#ROOT.gROOT.LoadMacro("AtlasUtils.C")
SetAtlasStyle() #This command activates the AtlasStyle settings and will produce a print out.

# Run ROOT in the Batch mode. Why? Otherwise ROOT will produce pop-up plots on the go. Sometimes it
# might not able to access to graphic display and program might fail.  
gROOT.SetBatch(True)

jetptcut=">1000"

tmpVale=sys.argv
def Helper():
    print " "
    print " Usage: python Compare.py file1 file2 leg1 leg2 outfolder tagger1 tagger2"
    print " "
    sys.exit(1)
if len(sys.argv) <8: Helper()

odir= sys.argv[5]
tag = sys.argv[6]
tag2 = sys.argv[7]
########################################################################################
#########################################################################################
gSystem.Exec("mkdir -p "+odir)

infile1=sys.argv[1]
infile2=sys.argv[2]

leg1=sys.argv[3]
leg2=sys.argv[4]

f1=TFile(infile1,"R")
f2=TFile(infile2,"R")

# here we create the axes that we draw our ROC curves on
light=TH1F("b VS light","b VS light",100,0.0,1.0);
light.SetTitle(";b-jet efficiency;light-jet rejection;")
lightCurve=[]
light.SetMinimum(1e0)
light.SetMaximum(1e5)
light.GetXaxis().SetRangeUser(0.0,1.0)

# now we create the ROC curves
f1.cd()  # file within the file
curve = f1.Get(tag+"---bl")#"MV2c10---bl")
curve.SetLineColor(1)
curve.SetLineWidth(3)

f2.cd()
curve2 = f2.Get(tag2+"---bl")#MV2c10---bl")
curve2.SetLineColor(4)
curve2.SetLineStyle(2)
curve2.SetLineWidth(3)

# and legend
legend=TLegend(0.55,0.7,0.92,0.90)
legend.SetTextFont(42)
legend.SetTextSize(0.042)
legend.SetFillColor(0)
legend.SetLineColor(0)
legend.SetFillStyle(0)
legend.SetBorderSize(0)
legend.AddEntry(curve,leg1,"L")
legend.AddEntry(curve2,leg2,"L")
legend.Draw("SAME")

legend5=TLegend(0.2,0.77,0.2,0.85)
legend5.SetTextFont(62)
legend5.SetTextColor(1)
legend5.SetTextSize(0.043)
legend5.SetFillColor(0)
legend5.SetLineColor(1)
legend5.SetBorderSize(0)
legend5.AddEntry("","Jet pT"+jetptcut+" GeV","")
legend5.Draw("SAME")

legend6=TLegend(0.2,0.65,0.2,0.77)
legend6.SetTextFont(62)
legend6.SetTextColor(1)
legend6.SetTextSize(0.043)
legend6.SetFillColor(0)
legend6.SetLineColor(1)
legend6.SetBorderSize(0)
legend6.AddEntry("","nEvents: 10000","")
legend6.Draw("SAME")





# create ratio, plot with ROC on same canvas
myCx=TCanvas( "bVSl", "bVSl",800,800);

pad_1=TPad("pad_1", "up", 0., 0.35, 1., 1.);
pad_1.SetBottomMargin(0);
pad_1.Draw();   
pad_1.SetGridy()
pad_1.SetGridx()
pad_1.SetLogy()
pad_2=TPad("pad_2", "down", 0.0, 0.00, 1.0, 0.35);
pad_2.SetTopMargin(0);
pad_2.SetBottomMargin(0.28);
pad_2.Draw();
pad_2.SetGridy()
pad_2.SetGridx()
pad_1.cd()
light.Draw()
curve.Draw("C")
curve2.Draw("C")
legend.Draw("SAME")

legend5.Draw("SAME")
legend6.Draw("SAME")
SetAtlasStyle() #This command activates the AtlasStyle settings and will produce a print out.
# This line requires AtlasLabels.C. Its structure is (x-coord,y-coord,"mytext").
#  It creates properly styled "ATLAS mytext" for the plots at the specified coordinates.
ATLASLabel(0.2,0.85,"Internal");


myCx.Update()

pad_2.cd()

# create axes for ratio
ratio = light.Clone("ratio")
ratio.GetYaxis().SetTitle("Ratio: "+leg2+"/"+leg1)
ratio.GetYaxis().SetTitleOffset(1.1)
ratio.GetXaxis().SetLabelSize(0.10)
ratio.GetXaxis().SetTitleSize(0.10)
ratio.GetYaxis().SetTitleOffset(0.7)
ratio.GetYaxis().SetLabelSize(0.09)
ratio.GetYaxis().SetTitleSize(0.09)
ratio.SetMaximum(3.3) ################################################
ratio.SetMinimum(0.01) ################################################
ratio.SetLineColor(kGray)
ratio.Draw("HIST")

# now we create the ratio curve
ratC = curve.Clone("ratioC")
countPoint = 0

print(" looping over: " + str(curve.GetN()) + "  points")

for mybin in xrange(0,curve.GetN()):  # really necessary to add 1?
    px1=Double(0)
    py1=Double(0)
    curve.GetPoint(mybin, px1, py1)
    # the other script calculates py2 from px1 so we'll do that as well
    clo_py2 = curve2.Eval(px1)
    ratC.SetPoint(countPoint, px1, clo_py2/py1) #so ratio is curve 2 / curve 1
    #ratC.SetPoint(countPoint, px1, clo_py1/py2) #so ratio is curve 2 / curve 1
    if "Errors" in ratC.ClassName(): ratC.SetPointError(countPoint,0,0)
    countPoint+=1

ratC.Draw("C")
myCx.Update()

myCx.Print(odir+"/bVSlight__"+tag+tag2+".png") #MV2c10
myCx.Print(odir+"/bVSlight__"+tag+tag2+".eps") #MV2c10

#input("Press Enter to Continue")


# we compare by looping over points of curve and curve2
'''
for bin1 in xrange(0,curve.GetN()): # includes under/overflow bins
    px1=Double(0)
    py1=Double(0)
    curve.GetPoint(bin1,px1,py1)
    #px2=Double(0)
    #py2=Double(0)
    #curve2.GetPoint(bin1,px2,py2)
    clopy2 = curve2.Eval(px1)
    #if px1 != px2: print("Not the same")
    print(py1 - clopy2)
'''
