setupATLAS

asetup AthDerivation,21.2.29.0,here

mkdir -p run

cd run

for JO in ../btagAnalysis/share/*.py; do
    ln -sf $JO
done

cd ..

if [[ ! -d build ]] ; then
    ./scripts/build.sh
else
    echo 'already built, run `./scripts/build.sh` to rebuild'
fi

source build/x86_64-slc6-gcc62-opt/setup.sh 
